use std::ptr::null_mut;
use std::fmt::{Debug, Formatter, Result};

struct Node<T> {
    data: T,
    prev: *mut Node<T>,
    next: *mut Node<T>
}

impl<T> Node<T> {
    fn new(data: T) -> Self {
        Self {
            data,
            prev: null_mut(),
            next: null_mut()
        }
    }

    fn next(&self) -> Option<*mut Self> {
        if self.next.is_null() {
            None 
        } else {
            Some(self.next)
        }
    }
}

impl<T: Debug> Debug for Node<T> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let next = match self.next() {
            None    => "None".to_owned(),
            Some(_) => "Some".to_owned()
        };

        write!(f, "{{ data: {:?}, next: {} }}", self.data, next)
    }
}

pub struct Deque<T> {
    root: *mut Node<T>,
    tail: *mut Node<T>,
    size: usize
}

impl<T> Deque<T> {
    pub fn new() -> Self {
        Self {
            root: null_mut(),
            tail: null_mut(),
            size: 0
        }
    }

    pub fn push(&mut self, data: T) {
        if self.root.is_null() {
            self.root = mem!(new Node::new(data));
            self.tail = self.root;
        } else {
            let mut boxed_tail = mem!(box self.tail);
            boxed_tail.next = mem!(new Node::new(data));
            boxed_tail.prev = self.tail;
            self.tail = boxed_tail.next;
            let _ = mem!(raw boxed_tail); // don't drop box
        }

        self.size += 1;
    }

    pub fn pop_queue(&mut self) -> Option<T> {
        if self.root.is_null() {
            return None;
        }

        let mut boxed_root = mem!(box self.root);
        boxed_root.prev = null_mut();
        self.root = boxed_root.next;
        self.size -= 1;
        Some(boxed_root.data)
        // boxed_root gets destroyed when out of scope
    }

    pub fn pop_stack(&mut self) -> Option<T> {
        if self.tail.is_null() {
            return None;
        }

        let mut boxed_tail = mem!(box self.tail);
        boxed_tail.next = null_mut();
        self.tail = boxed_tail.prev;
        self.size -= 1;
        Some(boxed_tail.data)
        // boxed_root gets destroyed when out of scope
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }
}

impl<T: Debug> Debug for Deque<T> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut tmp = self.root;

        write!(f, "[")?;

        while !tmp.is_null() {
            let tmp_box = mem!(box tmp);
            write!(f, " {:?} ", *tmp_box)?;

            tmp = tmp_box.next;                
            let _ = mem!(raw tmp_box);
        }

        write!(f, "]")
    }
}


