pub mod simple {
    use std::ptr::null_mut;
    use std::{
        error,
        fmt::{self, Display, Debug, Formatter}
    };

    #[derive(Clone, Debug)]
    pub enum ErrorKind {
        OutOfBound
    }

    #[derive(Clone, Debug)]
    pub struct Error<'a> {
        kind: ErrorKind,
        message: &'a str
    }

    impl<'a> Display for self::Error<'a> {
        fn fmt(&self, f: &mut Formatter) -> fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl<'a> error::Error for self::Error<'a> {
        fn description(&self) -> &str {
            match self.kind {
                ErrorKind::OutOfBound => "Index is out of bound"
            }
        }
    }

    struct Node<T> {
        data: T,
        next: *mut Node<T>
    }

    impl<T> Node<T> {
        fn new(data: T) -> Self {
            Self {
                data,
                next: null_mut()
            }
        }

        fn next(&self) -> Option<*mut Self> {
            if self.next.is_null() {
                None
            } else {
                Some(self.next)
            }
        }
    }

    impl<T: Debug> Debug for Node<T> {
        fn fmt(&self, f: &mut Formatter) -> fmt::Result {
            let next = match self.next() {
                None    => "None".to_owned(),
                Some(_) => "Some".to_owned()
            };

            write!(f, "{{ data: {:?}, next: {} }}", self.data, next)    
        }
    }

    pub struct List<T> {
        root: *mut Node<T>,
        tail: *mut Node<T>,
        size: usize
    }

    impl<T> List<T> {
        pub fn new() -> Self {
            Self {
                root: null_mut(),
                tail: null_mut(),
                size: 0
            }
        }

        pub fn push(&mut self, data: T) {
            if self.root.is_null() {
                self.root = mem!(new Node::new(data));
                self.tail = self.root;
            } else {
                let mut boxed_tail = mem!(box self.tail);
                boxed_tail.next = mem!(new Node::new(data));
                self.tail = boxed_tail.next;
                let _ = mem!(raw boxed_tail); // don't drop box
            }

            self.size += 1;
        }

        pub fn insert(&mut self, data: T, index: usize) -> Result<(), self::Error> {
            if index > self.size {
                return Err(Error { 
                    kind: ErrorKind::OutOfBound, 
                    message: "Trying to insert with index out of bound" 
                });
            }

            if self.root.is_null() {
                self.root = mem!(new Node::new(data));
                self.tail = self.root;
            } else {
                let mut tmp = self.root;
                for _ in 0..index {
                    tmp = unsafe { (*tmp).next };
                }

                let mut boxed_tmp = mem!(box tmp);
                let mut new_node = Box::new(Node::new(data));
                new_node.next = boxed_tmp.next;
                boxed_tmp.next = mem!(raw new_node);
                let _ = mem!(raw boxed_tmp); // don't drop box
            }

            Ok(())
        }

        pub fn len(&self) -> usize {
            self.size
        }

        pub fn is_empty(&self) -> bool {
            self.size == 0
        }
    }
}

pub mod double {

}
