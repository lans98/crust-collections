use std::ptr::null_mut;
use std::fmt::{self, Debug, Formatter};

struct Node<T> {
    data: T,
    next: *mut Node<T>
}

impl<T> Node<T> {
    fn new(data: T) -> Self {
        Node {
            data,
            next: null_mut() 
        }
    }

    fn next(&self) -> Option<*mut Self> {
        if self.next.is_null() {
            None
        } else {
            Some(self.next)
        }
    }
}

impl<T: Debug> Debug for Node<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let next = match self.next() {
            None    => "None".to_owned(),
            Some(_) => "Some".to_owned()
        };

        write!(f, "{{ data: {:?}, next: {} }}", self.data, next)
    }
}

pub struct Queue<T> {
    root: *mut Node<T>,
    tail: *mut Node<T>,
    size: usize
}

impl<T> Queue<T> {
    pub fn new() -> Self {
        Self {
            root: null_mut(),
            tail: null_mut(),
            size: 0
        }
    }

    pub fn push(&mut self, data: T) {
        if self.root.is_null() {
            self.root = mem!(new Node::new(data));
            self.tail = self.root;
        } else {
            let mut boxed_tail = mem!(box self.tail);
            boxed_tail.next = mem!(new Node::new(data));
            self.tail = boxed_tail.next;
            let _ = mem!(raw boxed_tail); // don't drop box
        }

        self.size += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.root.is_null() {
            return None;
        }

        let boxed_root = mem!(box self.root);
        let tmp = boxed_root.next;
        self.root = tmp;
        self.size -= 1;
        Some(boxed_root.data)
        // boxed_root gets destroyed when out of scope
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }
}

impl<T: Debug> Debug for Queue<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut tmp = self.root;

        write!(f, "[")?;

        while !tmp.is_null() {
            let tmp_box = mem!(box tmp);
            write!(f, " {:?} ", *tmp_box)?;

            tmp = tmp_box.next;                
            let _ = mem!(raw tmp_box);
        }

        write!(f, "]")
    }
}

