#[macro_use] 
pub(crate) mod mem;

pub mod list;
pub mod stack;
pub mod queue;
pub mod deque;
pub mod binary_tree;

pub mod prelude {
    pub use stack::Stack;
    pub use queue::Queue;
    pub use deque::Deque;
    pub use binary_tree::BinaryTree;
}

#[cfg(test)]
mod tests {

    #[test]
    fn stack_test() {
        use stack::Stack;

        let mut stack = Stack::new();
        stack.push(3);
        stack.push(5);
        stack.push(8);
        stack.push(11);
        stack.push(19);
        println!("Stack: {:?}", stack);

        if let Some(top) = stack.pop() {
            println!("Stack popped: {}", top);
        } else {
            println!("Stack is empty!");
        }
        println!("Stack: {:?}", stack);

    }

    #[test]
    fn queue_test() {
        use queue::Queue;

        let mut queue = Queue::new();
        queue.push(3);
        queue.push(5);
        queue.push(8);
        queue.push(11);
        queue.push(19);
        println!("Queue: {:?}", queue);

        if let Some(top) = queue.pop() {
            println!("Queue popped: {}", top);
        } else {
            println!("Queue is empty!");
        }

        println!("Queue: {:?}", queue);
    }

    #[test]
    fn deque_test() {
        use deque::Deque;

        let mut deque = Deque::new();
        deque.push(3);
        deque.push(5);
        deque.push(8);
        deque.push(11);
        deque.push(19);
        println!("Deque: {:?}", deque);

        if let Some(top) = deque.pop_stack() {
            println!("Deque popped stack: {}", top);
        } else {
            println!("Deque is empty!");
        }

        println!("Deque: {:?}", deque);

        if let Some(top) = deque.pop_queue() {
            println!("Deque popped queue: {}", top);
        } else {
            println!("Deque is empty!");
        }

        println!("Deque: {:?}", deque);
    }

    #[test]
    fn binary_tree_test() {
        use binary_tree::BinaryTree;

        let mut bt = BinaryTree::new();
        bt.insert(10).unwrap();
        bt.insert(9).unwrap();
        bt.insert(4).unwrap();
        bt.insert(1).unwrap();
        bt.insert(3).unwrap();

        println!("BinaryTree: {:?}", bt);
    }
}
